# LatinSlangKt
Inspired by [Common Mistake Developers Make When Building RecyclerViews and How to Fix It Using “ViewRepresentation” ](https://medium.com/swlh/common-mistake-developers-make-when-building-recyclerviews-and-how-to-fix-it-using-3b642a2ba0f4)

This is a demo about Recyclerview which show data( 5000 items ) from http api.

Reference:
[Scrolling performance of RecyclerView](https://medium.com/@kamilbekar/recyclerview-scrolling-performance-ff05a3a79262)
