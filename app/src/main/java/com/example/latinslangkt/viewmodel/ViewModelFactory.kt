package com.example.latinslangkt.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.latinslangkt.MyRepository
import kotlin.IllegalArgumentException

@Suppress("UNCHECKED_CAST")
class ViewModelFactory(private val repo: MyRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LatinSlangViewModel::class.java)) {
            return LatinSlangViewModel(repo) as T
        } else {
            throw IllegalArgumentException("Unknown viewModel ${modelClass.name}")
        }
    }
}