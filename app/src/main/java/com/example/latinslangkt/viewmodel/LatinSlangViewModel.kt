package com.example.latinslangkt.viewmodel

import androidx.lifecycle.*
import com.example.latinslangkt.MyRepository
import com.example.latinslangkt.model.LatinSlang
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class LatinSlangViewModel(
    private val repo: MyRepository,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) : ViewModel() {
    private val _latinSlangList = MutableLiveData<List<LatinSlang>>()
    val latinSlangList: LiveData<List<LatinSlang>> = _latinSlangList
    val isLoading = MutableLiveData<Boolean>()

    init {
        getLatinSlang()
    }

    private fun getLatinSlang() = viewModelScope.launch(dispatcher) {
        _latinSlangList.postValue(
            repo.getLatinSlangList()
        ).also { isLoading.postValue(false) }
    }

}