package com.example.latinslangkt

import android.graphics.Color
import androidx.annotation.ColorInt
import java.util.*

object ColorGenerator {
    private val rd = Random()

    @ColorInt
    fun getRandomColor(): Int {
        val a = getOne(200, 255)
        var r = getOne(100, 255)
        var g = getOne(100, 255)
        var b = getOne(100, 255)
        when {
            isMostBig(r, g, b) -> {
                g /= 2
                b /= 2
            }
            isMostBig(g, r, b) -> {
                r /= 2
                b /= 2
            }
            isMostBig(b, r, g) -> {
                r /= 2
                g /= 2
            }
        }
        return Color.argb(a, r, g, b)
    }

    private fun getOne(min: Int, max: Int): Int {
        return rd.nextInt(max - min) + min
    }

    private fun isMostBig(target: Int, num1: Int, num2: Int) = target > num1 && target > num2

}