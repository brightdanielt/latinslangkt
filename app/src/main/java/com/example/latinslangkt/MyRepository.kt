package com.example.latinslangkt

import com.example.latinslangkt.model.LatinSlang
import com.example.latinslangkt.network.ApiService

class MyRepository(
    private val apiService: ApiService
) {
    suspend fun getLatinSlangList(): List<LatinSlang> {
        return apiService.getLatinSlang()
    }
}