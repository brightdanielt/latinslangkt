package com.example.latinslangkt.ui

import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.latinslangkt.MyApplication
import com.example.latinslangkt.viewmodel.ViewModelFactory
import kotlinx.android.synthetic.main.activity_latin_slang.*

fun AppCompatActivity.getViewModelFactory(): ViewModelProvider.Factory {
    return ViewModelFactory((application as MyApplication).repo)
}

fun AppCompatActivity.showLoading(isLoading: Boolean) {
    when (isLoading) {
        true -> {
            pgBar_loading.visibility = VISIBLE
        }
        false -> {
            pgBar_loading.visibility = INVISIBLE
        }
    }
}