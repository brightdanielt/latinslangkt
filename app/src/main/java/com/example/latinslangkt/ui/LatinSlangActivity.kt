package com.example.latinslangkt.ui

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.example.latinslangkt.R
import com.example.latinslangkt.viewmodel.LatinSlangViewModel
import kotlinx.android.synthetic.main.activity_latin_slang.*

class LatinSlangActivity : AppCompatActivity() {

    private val viewModel: LatinSlangViewModel by viewModels { getViewModelFactory() }
    private lateinit var adapter: LatinSlangAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_latin_slang)

        setRv()
        subscribeViewModel()
    }

    private fun setRv() {
        adapter = LatinSlangAdapter()
        /*If the item counts is mutable, setting hasFixedSize makes ui work less,
        however the length is fix in this example.*/
        //latinSlangRv.hasFixedSize()
        latinSlangRv.adapter = adapter
    }

    private fun subscribeViewModel() {
        viewModel.isLoading.observe(this, {
            showLoading(it)
        })

        viewModel.latinSlangList.observe(this, {
            tv_hint.text = String.format(getString(R.string.rv_hint), it.size)
            adapter.swapData(it)
        })
    }

}