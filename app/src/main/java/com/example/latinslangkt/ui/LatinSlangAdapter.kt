package com.example.latinslangkt.ui

import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.latinslangkt.ColorGenerator
import com.example.latinslangkt.R
import com.example.latinslangkt.model.LatinSlang
import com.squareup.picasso.Picasso
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.latin_slang_item.view.*

class LatinSlangAdapter : RecyclerView.Adapter<LatinSlangAdapter.ViewHolder>() {

    private var dataList = listOf<LatinSlang>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.latin_slang_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(dataList[position])
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    class ViewHolder(
        override val containerView: View
    ) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        fun bind(item: LatinSlang) {
            itemView.tv_idContent.text = item.id.toString()
            itemView.tv_albumIdContent.text = item.albumId.toString()
            itemView.tv_title.hint = item.thumbnailUrl
            itemView.tv_title.text = item.title

            Picasso.get()
                .load(item.thumbnailUrl)
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.error)
                .into(itemView.img_thumbnail)

            /*val border =
                GradientDrawable().also { it.setStroke(3, ColorGenerator.getRandomColor()) }
            containerView.background = border*/
        }
    }

    fun swapData(list: List<LatinSlang>) {
        this.dataList = list
        notifyDataSetChanged()
    }

}
