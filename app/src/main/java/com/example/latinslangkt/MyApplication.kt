package com.example.latinslangkt

import android.app.Application
import com.example.latinslangkt.network.NetworkModule

class MyApplication : Application() {
    val repo: MyRepository by lazy { MyRepository(NetworkModule.apiService) }
}