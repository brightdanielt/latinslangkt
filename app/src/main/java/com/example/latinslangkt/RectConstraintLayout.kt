package com.example.latinslangkt

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout

/**
 * Created by Daniel Tsai on 2020/9/17
 * A simple rectangle layout.
 */
class RectConstraintLayout(
    context: Context,
    attrs: AttributeSet
) : ConstraintLayout(context, attrs) {
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec)
    }
}