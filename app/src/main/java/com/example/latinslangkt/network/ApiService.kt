package com.example.latinslangkt.network

import com.example.latinslangkt.model.LatinSlang
import retrofit2.http.GET

interface ApiService {
    @GET("/photos")
    suspend fun getLatinSlang(): List<LatinSlang>
}